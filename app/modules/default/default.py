from datetime import datetime
from flask import Blueprint
from flask import render_template
from flask import session
from flask import redirect
from flask import request
from flask import g
from flask import flash
from ..forms import reg_form
from ..forms import cart_form
from ..models import user as UserRef
from functools import wraps

mod = Blueprint('default', __name__)

def login_required(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        if 'username' not in session:
            return redirect('/login')
        else:
            return func(*args, **kwargs)
    return wrapper 

@mod.route('/')
def index():
    if 'username' not in session:
        return redirect('/login')
    current_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    return render_template('default/index.html', server_time=current_time)

@mod.route('/login', methods=['GET'])
def login_page():
    if 'username' in session:
        return redirect('/')
    return render_template('default/login.html')

@mod.route('/login', methods=['POST'])
def login_submit():
    username = request.form['username']
    password = request.form['password']
    if g.usersdb.getUserWithPassword(username, password).count() > 0:
        session['username'] = username
        return redirect('/')
    else:
        flash('Invalid username and password.', 'signin_failure')
        return redirect('/login') 

@mod.route('/logout', methods=['GET'])
def logout_submit():
    session.pop('username', None)
    session.clear()
    return redirect('/')

@mod.route('/register', methods=['GET', 'POST'])
def register():
    form = reg_form.RegistrationForm(request.form)
    if request.method == 'POST' and form.validate():
        user = UserRef.User(form.username.data, form.email.data,
                    form.password.data)
        g.usersdb.createUserUsingUser(user)
        session['username'] = form.username.data
        return redirect('/')
    return render_template('default/signup.html', form=form)

@mod.route('/shopping-list', methods=['GET'])
def list_page():
    item_name = request.args.get('item_name')
    if item_name is not None:
        s_list = session["shopping_list"]
        s_list[item_name]["quantity"] += 1
        session["shopping_list"] = s_list
        total = 0
        for x in s_list.keys():
            total += s_list[x]["quantity"] * s_list[x]["price"]
        return render_template('list/list.html', shopping_list=s_list, list_total=total, shopping_cart=session["shopping_cart"], add_item_form=add_item_form, add_to_cart_form=add_to_cart_form)
    else:
        shopping_list = {
                            'car':{'price':50,'quantity':0},
                            'pen':{'price':5,'quantity':0},
                            'ball':{'price':10,'quantity':0},
                            'glass':{'price':10,'quantity':0}
                        }
        shopping_cart = {} 
        add_to_cart_form = cart_form.AddToCartForm(request.form)
        add_item_form = cart_form.AddItemForm(request.form)
        session["shopping_list"] = shopping_list
        session["shopping_cart"] = shopping_cart
        return render_template('list/list.html', shopping_list=shopping_list, cart_total=0,shopping_cart=shopping_cart,list_total=0, add_item_form=add_item_form, add_to_cart_form=add_to_cart_form)

@mod.route('/shopping-list', methods=['POST'])
def add_to_list():
    item_name = request.form["item_name"]
    price = request.form["price"]
    s_list = session["shopping_list"]
    cart = session["shopping_cart"]
    add_to_cart_form = cart_form.AddToCartForm(request.form)
    add_item_form = cart_form.AddItemForm(request.form)
    cart_total = 0
    list_total = 0
    for key in cart.keys():
        cart_total += int(cart[key]["price"])*int(cart[key]["quantity"])
    for key in s_list.keys():
        list_total += int(s_list[key]["price"])*int(s_list[key]["quantity"])
    if add_item_form.validate():
        if item_name in s_list.keys():
            return render_template('list/list.html', shopping_list=s_list, cart_total=cart_total,shopping_cart=cart,list_total=list_total, add_item_form=add_item_form, add_to_cart_form=add_to_cart_form, add_to_list_err=True)
        else:
            s_list[item_name] = {'price':price,'quantity':1}
            list_total += int(price)
            return render_template('list/list.html', shopping_list=s_list, cart_total=cart_total,shopping_cart=cart,list_total=list_total, add_item_form=add_item_form, add_to_cart_form=add_to_cart_form)
    return render_template('list/list.html', shopping_list=s_list, cart_total=cart_total,shopping_cart=cart,list_total=list_total, add_item_form=add_item_form, add_to_cart_form=add_to_cart_form)

@mod.route('/add-to-cart',methods=["POST"])
def add_to_cart():
    item_to_add = request.form["item_name"]
    cart = session["shopping_cart"]
    s_list = session["shopping_list"]
    add_to_cart_form = cart_form.AddToCartForm(request.form)
    add_item_form = cart_form.AddItemForm(request.form)
    cart_total = 0
    list_total = 0
    for key in cart.keys():
        cart_total += int(cart[key]["price"])*int(cart[key]["quantity"])
    for key in s_list.keys():
        list_total += int(s_list[key]["price"])*int(s_list[key]["quantity"])
    if add_to_cart_form.validate():
        if item_to_add in cart.keys():
            cart[item_to_add]["quantity"] += int(request.form["quantity"])
            cart_total += int(request.form["quantity"]) * cart[item_to_add]["price"]
        else:
            cart[item_to_add] = {'price':0,'quantity':0}
            cart[item_to_add]["price"] = int(s_list[item_to_add]["price"])
            cart[item_to_add]["quantity"] = int(request.form["quantity"])
            cart_total += int(s_list[item_to_add]["price"])*int(request.form["quantity"])
            s_list.pop(item_to_add)
        session["shopping_cart"] = cart
        return render_template('list/list.html', shopping_cart=cart,shopping_list=s_list, cart_total=cart_total, list_total=list_total, add_item_form=add_item_form,add_to_cart_form=add_to_cart_form)
    return render_template('list/list.html', shopping_cart=cart,shopping_list=s_list, cart_total=cart_total, list_total=list_total, add_item_form=add_item_form,add_to_cart_form=add_to_cart_form)
