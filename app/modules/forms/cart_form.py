from wtforms import Form
#from wtforms import BooleanField
from wtforms import TextField
from wtforms import FloatField
from wtforms import IntegerField
#from wtforms import PasswordField
from wtforms import validators
from wtforms import ValidationError
from wtforms import FieldList

class AddItemForm(Form):
    item_name = TextField(label='Item Name', validators=[validators.Length(min=10, max=50)])
    price = FloatField(label='Price', validators=[validators.NumberRange(min=5)], default=10)
    
class AddToCartForm(Form):
	#quantity = FieldList(unbound_field=IntegerField(label="Item quantity", validators=[validators.NumberRange(min=1)], default=1), min_entries=1)
	quantity = IntegerField(label='Item quantity', validators=[validators.NumberRange(min=1)], default=1)

